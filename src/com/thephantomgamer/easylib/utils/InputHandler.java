package com.thephantomgamer.easylib.utils;

import java.util.Scanner;

/**
 * <p>This class contains the getInput method, a method to make acquiring, storing, and manipulating user input easier.</p>
 * The project easylib was
 * Created by ThePhantomGamer on 11/29/2015.
 */
public class InputHandler {
    private Scanner scanner = new Scanner(System.in);

    /**
     * <p>This method makes getting user input easier</p>
     * <p>It first prints out the prompt to the user, then, using the Scanner class,
     * it scans for nextLine().</p>
     * <p>Additionally, it adds a colon to make the prompt look nicer if there isn't already one.</p>
     * <strong style="font-size:15pt">Possible Usage</strong>
     * <br>
     * <code>String name = inputHandler.getInput("Enter your name");</code>
     * @param prompt The message you want to prompt the user with
     * @return Returns the user's input
     */
    public String getInput(String prompt){

        //Adds a nice : to the end of the prompt if their isn't one
        if(!prompt.endsWith(":")){
            prompt = prompt + ":";
        }

        System.out.println(prompt);
        String userInput = scanner.nextLine();
        return userInput;
    }
    //TODO overload method for not having a colon at the end?

}
