package com.thephantomgamer.easylib.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;

/**
 * The project easylib was
 * Created by ThePhantomGamer on 3/11/2016.
 * This file, ImageHelper was created on 6:58 PM.
 */
public class ImageHelper {

    /**
     * This method gets and image from a path
     * <p style="font-size:14px; font-weight=3px">Note: the image must be in classpath, otherwise this will fail</p>
     * @param pathToImage The path of the image file
     * @return Returns the image
     */
    public Image getImage(String pathToImage){
        Image image = null;
        try {
            image = ImageIO.read(this.getClass().getResource(pathToImage));
        } catch (IOException e) {
            System.err.println("getImage() failed");
            e.printStackTrace();
        }
        return image;
    }



}
