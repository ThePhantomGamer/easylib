package com.thephantomgamer.easylib.utils;

import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 * The project easylib was
 * Created by ThePhantomGamer on 8/20/2016.
 * This file, JFXWindowHelper was created on 9:43 AM.
 */
public class JFXWindowHelper {


    public static void showWindow(String text, int width, int height){

        AnchorPane layout = new AnchorPane();
        Scene scene = new Scene(layout, width, height);
        Stage stage = new Stage();

        Text sceneText = new Text(text);

        sceneText.setFont(new Font("Segoe UI", 20));
        sceneText.setTextAlignment(TextAlignment.LEFT);

        sceneText.setY(height/9);
        sceneText.setX(20);

        stage.setResizable(false);


        // How to add items from code
        layout.getChildren().add(sceneText);


        stage.setScene(scene);
        stage.show();
    }

}
