package com.thephantomgamer.easylib.utils;

import com.thephantomgamer.easylib.misc.FormatType;

import java.text.NumberFormat;

/**
 * <p>This class contains the format() method, a method to make formatting number easier</p>
 * The project easylib was
 * Created by ThePhantomGamer on 11/26/2015.
 */
public class NumberFormatter {

    //Java's number formatter
    NumberFormat numberFormatter = NumberFormat.getInstance();

    /**
     * <p>The format method makes formatting numbers much more easier</p>
     * <p>First it checks for which format of the number you want it to be.
     * Then, it uses String.format to format the number</p>
     * <strong style="font-size:15pt">Possible Usage</strong>
     * <br>
     * <code>System.out.println(numberFormatter.format(251, FormatType.SERIES)); //Returns 251st</code>
     * @param number The number you want to format
     * @param format The type of formatting you want
     * @return Returns the formatted number
     */
    public String format(double number, FormatType format){

        String formattedNumber = "";
        if(format.equals(FormatType.COMMA_SEPARATED)){

            //System.out.println("Formatting number with comma seperation");
            formattedNumber = String.format("%,.0f", number);

        }

        else if(format.equals(FormatType.PERCENTAGE)){

            /*System.out.println("Formatting number with percentages");*/
            formattedNumber = String.format("%.0f", number) + "%";

        }

        else if(format.equals(FormatType.SERIES)){

            //System.out.println("Formatting number in series format");
            //Temporary String wrapper for number to see if it ends with one
            if(String.format("%.0f", number).endsWith("1")){ //Hard to grasp at first
                formattedNumber = String.format("%,.0f", number) + "st";
            } else if (String.format("%.0f", number).endsWith("2")){
                formattedNumber = String.format("%,.0f", number) + "nd";
            } else if (String.format("%.0f", number).endsWith("3")){
                formattedNumber = String.format("%,.0f", number) + "rd";
            } else {
                formattedNumber = String.format("%,.0f", number) + "th";
            }

        } else if (format.equals(FormatType.DECIMAL)){
            //Fallback
            System.err.println("Error: Please provide the precision of the end result after the format...");
        } else {
            System.err.println("Error: Invalid numbering format.\nPlease use a correct format. See FormatType Enum for list");
        }

        return formattedNumber;
    }

    /**
     *<p>This method provides the support for decimals</p>
     * @param number The number you want to format
     * @param format The type of formatting you want
     * @param precision The decimal precision of the number
     * @return Returns the formatted number
     */
    public String format(double number, FormatType format, float precision){

        String formattedNumber = "";
        //Safety check to see if the person put the precision more than 0.9f
        if (format.equals(FormatType.DECIMAL)){
            if(precision >= 1.0f){
                precision = .9f;
            }

            //deletes the 0 from "0.9f" because String.format doesn't like it
            StringBuilder sb = new StringBuilder(Float.toString(precision));
            sb.deleteCharAt(0);
            String precisionString = sb.toString();

            //Creates the formatting with the custom precision
            String customFormat = "%," + precisionString + "f";
            formattedNumber = String.format(customFormat, number);
        } else {
            System.err.println("Error: Invalid numbering format.\n" + "Please use a correct format. See the FormatType Enum for list");
        }

        return formattedNumber;
    }

}
