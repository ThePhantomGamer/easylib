package com.thephantomgamer.easylib.utils;

/**
 * The project easylib was
 * Created by ThePhantomGamer on 1/7/2016.
 * This file, TextHelper was created on 8:40 PM.
 */
public class TextHelper {


    public static String reverseString(String aString){
        StringBuilder t1Wrapper = new StringBuilder(aString);

        //Reverse the first string
        aString = t1Wrapper.reverse().toString();

        //Check if the reversed string is a palindrome
        return aString;
    }
    
}
