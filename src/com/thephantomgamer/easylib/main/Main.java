package com.thephantomgamer.easylib.main;

import com.thephantomgamer.easylib.misc.Constants;
import com.thephantomgamer.easylib.misc.FormatType;
import com.thephantomgamer.easylib.utils.ImageHelper;
import com.thephantomgamer.easylib.utils.InputHandler;
import com.thephantomgamer.easylib.utils.NumberFormatter;
import com.thephantomgamer.easylib.utils.TextHelper;

import java.awt.*;


/**
 * The Main class contains some usage examples
 * The project easylib was
 * Created by ThePhantomGamer on 11/26/2015.
 */
public class Main {

    public static void main(String args[]){

        NumberFormatter numberFormatter = new NumberFormatter();
        InputHandler inputHandler = new InputHandler();
        TextHelper textHelper = new TextHelper();
        ImageHelper imageHelper = new ImageHelper();


        /*String str1 = "cat";
        String str2 = "tac";
        String str3 = "dog";*/

        System.out.println("Using version" + " " + Constants.VERSION);

        //Examples

        //NumberFormatter
        System.out.println(numberFormatter.format(251, FormatType.SERIES)); //Returns 251st

        //Input
        String username = inputHandler.getInput("Give username plz");
        String userpass = inputHandler.getInput("Give pass plz");
        System.out.println("Your Username is " + username + " and Your Password is " + userpass);

        //
        String str1 = inputHandler.getInput("Select your first string");
        String str2 = inputHandler.getInput("Select your second string");

        if (TextHelper.reverseString(str1).equals(str2)) {
            System.out.println("The Strings " + "\"" + str1 + " \" and \"" + str2 + "\" are the same backwards.");
        } else {
            System.out.println("The Strings " + "\"" + str1 + " \" and \"" + str2 + "\" are not the same backwards.");
        }

        //Get an image and do something with it
        Image anImage = imageHelper.getImage("/syncing.png");

    }
}
