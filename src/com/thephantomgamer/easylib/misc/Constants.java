package com.thephantomgamer.easylib.misc;

/**
 * <p>This class contains Constants that are used globally.</p>
 * The project easylib was
 * Created by ThePhantomGamer on 11/30/2015.
 * <p>However, it only contains the version as of now.</p>
 */
public class Constants {
    /**
     * The version float is only changed while editing the code
     * Current version: 1.4
     */
    public static final float VERSION = 1.4f;

    /*
     * Version history:

     * v1.0 - 11/30/15 6:12 PM officially EST -Official Release
     * v1.1 - 12/3/15 5:20 PM EST --Add decimal support
     * v1.2 - 12/12/15 9:06 PM EST
     * v1.3 - 1/7/16 9:07 PM EST --Add a method to check if strings are the same reversed
     * v1.4 - 1/8/16 10:57 PM EST --Finally setup git... will not be updating this anymore
     */
}
