package com.thephantomgamer.easylib.misc;

/**
 * <p>Holds all the types that the library can currently format</p>
 * The project easylib was
 * Created by ThePhantomGamer on 11/26/2015.
 * <br>
 * <strong>Currently the formats are</strong>
 * <ul>
 *     <li>Ordinal Numbers <strong>FormatType.SERIES</strong></li>
 *     <li>Comma Separated <strong>FormatType.COMMA_SEPARATED</strong></li>
 *     <li>Percentage <strong>FormatType.PERCENTAGE</strong></li>
 *     <li>Decimal <strong>FormatType.DECIMAL</strong></li>
 * </ul>
 */
public enum FormatType {
    //TODO ADD MOAR FORMATS
    SERIES, COMMA_SEPARATED, PERCENTAGE, DECIMAL
}
